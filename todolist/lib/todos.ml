let connection_url = "postgresql://localhost:5432"

(* this is connection pool we will use for executing db operation *)
let pool =
  match Caqti_lwt.connect_pool ~max_size:10 (Uri.of_string connection_url) with
  |Ok pool -> pool
  |Error err -> failwith (Caqti_error.show err)

type todo = {
    id : int;
    content :string;
  }
type error = {
  |Database_error of string
  }

let or_error m =
  | Ok a -> Ok a |> Lwt.return
  | Error e -> Error (Database_error (Caqti_error.show e)) |> Lwt.return

let migrate_query =
  Caqti_request.exec
