open Base
open Stdio


let () =
  let replace input output  =
    Str.global_replace  (Str.regexp input) output
  in
  printf "source ./base.sh \n";
  In_channel.input_lines stdin
  |> List.iter ~f:(fun (line ) -> printf "%s\n"
                                    (replace "@require \\([a-zA-Z]+\\)"
                                       "source ./\\1.sh" line));
  printf "edownload\n";
  printf "econfigure\n";
  printf "emake\n";
  printf "einstall\n";
  printf "epost-install\n"
