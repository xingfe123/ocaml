let top_k input k =
  let n = Array.length input in
  let indices  = Array.init n (fun x -> x) in
  for i = 0 to (k-1) do
    for j = (n-1) downto 1 do
      if input.(indices.(j-1)) > input.(indices.(j)) then begin
        let b = indices.(j-1) in
        indices.(j-1) <- indices.(j);
        indices.(j) <-b ;
      end
    done;
  done;
  Array.sub indices 0 k

let nearest_neighbours point points k distance =
  let distances = Array.map (fun x -> distance x point) points in
  top_k distances k

let predict neighbours labels =
  let sum a b = a +. b in
  let k = Array.length neighbours in
  if Array.fold_left sum 0. (Array.init k (fun i -> labels.(neighbours.(i)))) > 0.
  then 1
  else -1

let max_length = 1.
let chessboard_boundary x y
    if((mod_float x 0.5) -. 0.25) *. ((mod_float y 0.5) -. 0.25) > 0. then
      1.0
    else
      (-1.0)

let circle_boundary x y = if (x**2. +. y**2.) > 0.5 then 1. else (-1.)
let unlabelled_boundary x y = 2

let make_data n decision_boundary =
  let output = Array.init n (fun _ -> (Array.make 2 0.)) in
  let output_label = Array.make n_points 0. in
  for i = 0 to (n -1) do
    output.(i).(0) <- Random.float max_length ;
    output.(i).(1) <- Random.float max_length;
    output_label.(i) <- decision_boundary output.(i).(0) output.(i).(1)
  done;
  output, output_label

let sum a b = a +. b in
let euclide x y
let diff = Array.init (Array.length x)(fun i -> (x.(i) -. y.(i))**2.)in
Array.fold_left sum 0. diff

let manhattan x y
let diff = Array.init (Array.length x)(fun i -> abs (x.(i) -. y.(i)))in
Array.fold_left sum 0. diff

let n = int_of_string Sys.argv.(1);;
let k = int_of_string Sys.argv.(2);;
let n_test_points = 50;


let train, labels = make_data n circle_boundary

let test, pseudo = make_data n unlabelled_boundary



let nearest_neighbours = Array.map (fun x -> find_nearest_neighbours  x train_date k euclide_distance)   test_data

let mismatches = ref 0.;;

for l = 0 to (n_test_points-1 ) do
  pseudo_labels.(l) <- predict nearest_neighbours.(l) labels;
  if pseudo_labels.(l) <> (circle_boundary test_data.(l).(0) test_data.(l).(0))
  then (mismatches := !mismatches +. 1.) else();
done;;
