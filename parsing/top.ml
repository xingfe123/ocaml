(* open Lexing *)
open Lexer

 (* -当总浏览次数小于等于50万次时，和拉取1次浏览次数+1；
  *        -当总浏览次数，大于50万次时，小于100万次时，拉取2次，浏览次数+1；
  *        -当总浏览次数，大于等于100万次时，小于1000万次时，拉取5次，浏览次数+1；
  * -当总浏览次数，大于等于1000万次时，拉取10次，浏览次数+1； *)
let wang = 10000
let rec viewNumber number=
  if number <= 50*wang then
    number
  else if number <= 100*wang then
    (viewNumber (50*wang )) + (number - (50*wang))/2
  else if number <= 1000*wang  then
    (viewNumber (100*wang)) + (number-100*wang)/5
  else
    (viewNumber (10000*wang)) + (number - 10000*wang)/5

let rec parse_and_print lexbuf =
  try Parser.prog Lexer.read lexbuf with
  |Some value ->
    printf "%s\n" value;
    parse_and_print lexbuf
  |None -> ()

let () =
  let lexbuf = Lexing.from_channel (open_in "example.log") in
  Lwt_main.run (Lwt_io.printf "hello world %d\n" (viewNumber (52*wang)) );
  Lwt_main.run (Lwt_io.printf "hello world \n" );
  parse_and_print lexbuf
