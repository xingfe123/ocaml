%token <string> STRING
%token COMMA
%token EOF
%token SELECT
%token FROM

%start prog
%type <string> prog

%%

prog:
  | SELECT colums from { Some $2 }
from:
  |FROM table ; EOF {}
colum:
  | STRING { $1}
table:
  | STRING { $1}
colums:
  | colum {  $1}
  | colum COMMA colums { $1}
