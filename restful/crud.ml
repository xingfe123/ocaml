module Store = struct
  let create() =
    Lwt_mvar.create []

  let idx = ref 0
  let with_store db ~f =
    Lwt_mvar.take db
    >>= fun l ->
    let result, l' = f l in
    lwt_mvar.put db l' >|= fun () ->
    result

end

class items db = object(self)
  inherit [Cohttp_lwt.Body.t] Wm.resource
end

let main() =
  let port = 8080 in
  let db = Store.create() in
  let routes = [
    ("/items", fun () -> new items db);
    ("/items/:id", fun () -> new item db);
  ]in
  Server.create ~mode:(`TCP(`Port port)) config
  >>=(fun () -> Printf.eprintf)

let () = Lwt_main.run (main());
