{
  open Parser
}

rule lex = parse
  | [' ' '\t'] {lex lexbuf}
  | '\n' {lex lexbuf}
  | "package" { PACKAGE }
  |"import" { IMPORT }
  |"option" { OPTION }
  |"message" { MESSAGE }
  |"optional" { OPTIONAL }
  |"repeated" { REPEATED }
  |"uint64" { UINT64 }
  | "string" { STRING }
  |"uint32" { UINT32 }
  | "true" {TRUE}
  |  "=" {EQ}
  | "false" {FALSE}
  | ['a'-'z' ]* as s {ID (s)}
  | eof {EOF}
  | _ {print_endline ("error char" ^ (Lexing.lexeme lexbuf)) ; EOF }
