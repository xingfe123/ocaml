open Formula

let of_string f s =
  f Lexer.lex (Lexing.from_string s)
let package_of_string s =
  of_string  Parser.parse_package s


let import_of_string s = of_string Parser.parse_import s

let option_of_string s = of_string Parser.parse_option s

let field_of_string s = of_string  Parser.parse_field s




let _ =
  print_endline (package_string (package_of_string "package market"));
  print_endline (import_string (import_of_string "import market"));
  print_endline (option_string (option_of_string "option name = true"));
  print_endline (field_string (field_of_string "option name = true"));

(*
print
_endline (import_string (formula_of_string "import srpc.proto")) *)
