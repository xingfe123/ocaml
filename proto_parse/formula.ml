type package_expr = Package of string
type import_expr = Import of string
type option_expr = Option of string * bool
type type_expr =
  |Int32
  |String
  |Uint32
  |User_define of string

type base_field_expr =
  |Optional of type_expr * string * int
  |Required of type_expr * string * int
  |Repeated of type_expr * string * int

type field_expr =
  |Enum of string * string
  |Base of base_field_expr




type message_expr = Message of string * (field_expr list)
let package_string  = function
  |Package tablename -> "package " ^ tablename
let import_string = function
  |Import tablename -> "import " ^ tablename

let option_string = function
  |Option(v, b) -> "option " ^ v ^ " = " ^ (if b then "true" else "false")

let type_string = function
  |Int32 -> "int32"
  |String  -> "string"
  |Uint32 -> "uint32"
  |User_define(a) -> a

let field_string = function
  |Enum(a, b) -> ("enum {"^a^"=1; "^b^"=2}")
  |Base(expr)->
    match expr with
    |Optional(te, s, i) -> ("optional "^type_string(te)^" "^s^" = "^string_of_int(i)^"")
    |Required(te, s, i) -> ("required "^type_string(te)^" "^s^" = "^string_of_int(i)^"")
    |Repeated(te, s, i) -> ("repeated "^type_string(te)^" "^s^" = "^string_of_int(i)^"")
