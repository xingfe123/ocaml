%token <string> ID
%token TRUE FALSE
%token <int> NUMBER
%token <string> TYPE
%token  REQUIRED REPEATED OPTIONAL
%token PACKAGE IMPORT OPTION
%token MESSAGE ENUM

%token UINT64 STRING UINT32 
%token EQ EOF 

%{
    open Formula
%}

%start parse_package
%type <Formula.package_expr> parse_package

%start parse_import
%type <Formula.import_expr> parse_import

%start parse_option
%type <Formula.option_expr> parse_option

%start parse_field
%type <Formula.field_expr> parse_field

%%


%public parse_package:
  | PACKAGE ID   { Package($2)}
parse_import:
    | IMPORT ID  {Import ($2)}

parse_option:
    |OPTION ID EQ TRUE  { Option($2, true) }
    |OPTION ID EQ FALSE  { Option($2, false) }

parse_field:
    |ENUM ID ID {Enum($2,$3)}
    |OPTIONAL UINT32 ID NUMBER  {Base(Optional(Uint32, $2,$3))}



