{
type token =
  |ID of (string)
  |AND
  |OR
  |NOT
  |OP
  |CL
  |EOF
}

rule lex = parse
  | [' ' '\t'] {lex lexbuf}
  | "and" {AND}
  | "or" {OR}
  | "not" {NOT}
  | "(" {OP}
  | ")" {CL}
  | ['A'-'Z' ]* as s {ID (s)}
  | eof {EOF}
