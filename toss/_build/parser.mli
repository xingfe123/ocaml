
(* The type of tokens. *)

type token = Lexer.token

(* This exception is raised by the monolithic API functions. *)

exception Error

(* The monolithic API. *)

val parse_formula: (Lexing.lexbuf -> token) -> Lexing.lexbuf -> (Formula.formula)
