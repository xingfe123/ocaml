type formula =
  | Var of string
  | Not of formula
  | Or of formula * formula
  | And of formula * formula

let rec string1 = function
  |Var s -> s
  |Not f -> "(not " ^  (string1 f) ^  ")"
  |Or (f, g) -> "(or " ^  (string1 f) ^  ")"
  |And (f, g) -> "(or " ^  (string1 f) ^  ")"

let rec nnf ?(negate=false) = function
  | Var s -> if not negate then Var s else Not (Var s)
  | Not f -> nnf ~negate:(not negate) f
  | Or (f, g) ->  if not negate then Or (nnf f, nnf g) else
                   And (nnf ~negate:true f, nnf ~negate:true g)
  | And (f, g) -> if not negate then And (nnf f, nnf g) else
                    Or (nnf ~negate:true f , nnf ~negate:true g)
