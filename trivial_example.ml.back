module type X_int = sig val x : int end
module Increment (M : X_int) : X_int = struct
  let x = M.x + 1
end

module Three_and_more = struct
  let x = 3
  let y = "three"
end;;
module Four = Increment(Three_and_more)
(* module Four: sig val x : int end *)

module type Comparable =
sig
  type t
  val compare : t -> t -> int
end;;

module Create_interval(Endpoint : Comparable) = struct
  type t = | Interval of Endpoint.t * Endpoint.t
           | Empty

  let create low high
      if Endpoint.compare low high >0 then Empty
      else Interval (low, high)

  let is_empty = function
    |Empty -> true
    |Interval _ -> false

  let contains t x =
    match t with
    |Empty -> false
    |Interval(low, high) ->
      Endpoint.compare x l >=0 && Endpoint.compare x h <= 0

  let intersect t1 t2 =
    let min x y = if Endpoint.compare x y <= then x else y in
    let max x y = if Endpoint.compare x y >= 0 then x else y in
    match t1, t2 with
    |Empty, _| _, Empty -> Empty
    |Interval(l1, h1), Interval(l2,h2) ->
      create (max l1 l2) (min h1 h2)
end

module type Interval_intf = sig
  type t
  type endpoint
  val create : endpoint -> endpoint -> t
  val is_empty : t -> bool
  val is_empty : t-> bool
  val contains : t -> endpoint -> bool
  val intersect : t -> t -> t
end;;

module Int_interval = Make_interval(Int)

(** destructive substitution **)
module type Int_interval_intf = Interval_intf with type endpoint := int;

(** serialize **)
type some_type = int * string list with sexp ;;

module type Extension = sig
  type 'a t
  val iter : 'a t -> f:('a -> unit) -> unit
  val length : 'a t -> int
  val count : 'a t -> f:('a -> bool ) -> int
  val for_all : 'a t -> f:('a -> bool) -> bool
  val exists : 'a t -> f:('a -> bool) -> bool
end

module Extend(Arg :S)
  :(Extension with type 'a t := 'a Arg.t) =
struct
  open Arg
  let iter t ~f =
    fold t ~init:() ~f:(fun() a -> f a)

  let length t =
    fold t ~init:0 ~f:(fun acc _ -> acc + 1)
  let count t ~f
      fold t ~init:0 ~f:(fun acc _ -> acc + 1)

  exception Short_circuit

  let for_all c ~f =
    try iter c ~f:(fun x -> if not (f x ) then raise Short_circuit); true
    with Short_circuit -> false

  let exists c ~f =
    try iter c ~f:(fun x -> if f x then raise Short_circuit);false
    with Short_circuit->false

end


type 'a t
include (module type of Fqueue) with type 'a t := 'a t
include Foldable.Extension with type 'a t := 'a t



(* sharing constraint * )
module type Int_interval_intf = Int_interval with type endpoint = int;




module Int_interval =
  Create_interval(struct
    type t = int
    let compare = Int.compare
  end )
(* aligned with the standards *)
module String_interval = Make_interval(String);;
