open Cmdliner

let sum a b = a +b
let epsilon = 0.01
let close_enough left right =
  abs_float( left -. right) < epsilon

let capit () =
  Alcotest.(check char) "same chars"  'A' (Char.uppercase_ascii 'a')

let plus () =
  Alcotest.(check int) "same ints" 7 (List.fold_left sum 0 [1;1;2;3] )

let test_set = [
  "Capitalize" , `Quick, capit;
  "Add entries", `Slow , plus ;
]

let test_nice i = Alcotest.(check int) "Is it a nice Integer?" i 42

let iValue =
  let doc = "What is your prefered number?" in
  Arg.(required & opt (some int) None & info["n"] ~doc ~docv:"num")

let () =
  Alcotest.run_with_args "foor" iValue [
    "x", ["nicde", `Quick, test_nice]
  ];
  Alcotest.run "Haversine" [
    "calculate_distance BNA-LAX", test_set;
  ];
