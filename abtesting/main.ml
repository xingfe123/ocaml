open Opium.Std

type user = {
  idx : string;
  age: int;
}

let json_of_user{idx; age} =
  let open Ezjsonm in
  dict ["name", (string idx);
        "age", (int age)
       ]

let print_user = get "/user/:idx/:age" begin fun req ->
    let user = {
      idx = param req "idx";
      age = "age" |> param req |> int_of_string;
    } in
    `Json (user |> json_of_user) |> respond'
  end


let user_age = get "/user/:idx" begin fun _req ->
    `String (string_of_int(10)) |> respond'
  end




  

let () =
  print_endline "hello world!";
  App.empty
  |> print_user
  |> user_age
  |> App.run_command
  
