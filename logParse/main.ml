open Formula

let formula_of_string s =
  Parser.parse_select Lexer.lex (Lexing.from_string s)

let from_of_string s =
  Parser.parse_from Lexer.lex (Lexing.from_string s)


let _ =
  print_endline (from_string (from_of_string "from market"));
  print_endline (select_string (formula_of_string "select idx from market"))
