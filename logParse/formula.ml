type from_expr = From of string
type select = Select of (string list) * from_expr
let from_string  = function
  |From tablename -> "from " ^ tablename
let select_string = function
  |Select (colums, from_expr) ->
    "select " ^ (String.concat "," colums) ^ " "^(from_string from_expr)
