%token <string> ID
%token SELECT FROM OP EOF 

%{
    open Formula
%}

%start parse_from
%start parse_select
%type <Formula.from_expr> parse_from
%type <Formula.select> parse_select

%%


%public select_expr:
  |SELECT colums from_expr   {Select ($2, $3)}
from_expr:
  |FROM ID                     {From ($2)}
colums:
  |ID                  { [$1]}
  |ID OP colums        { $1::$3}
parse_from:
  |from_expr EOF {$1}
parse_select:
  |select_expr EOF                     {$1}
