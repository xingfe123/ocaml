{

open Parser
}

rule lex = parse
  | [' ' '\t'] {lex lexbuf}
  | "from" {FROM}
  | "select" {SELECT}
  | ['a'-'z' ]* as s {ID (s)}
  | eof {EOF}
