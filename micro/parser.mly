%token BEGIN
%token END EOF READ WRITE ASSIGN LEFT RIGHT ADD SUB COMMA SEMICOLON
%token <string> IDENRIFIER
%token <int> LITERAL

%start program
%type <unit> program

%{
open codegen
let depth = ref 0 
let depth_incr f = incr depth; f !depth 
let depth_rest = depth := 0 

%}
program:
| begin_stmt statements end_stmt EOF {raise End_of_file}
;

begin_stmt:
|BEGIN {generate_begin()}

end_stmt:
|END {generate_end()}

identifier_list:
| IDENTIFIER { [$1] }
| IDENTIFIER COMMA identifier_list { $1 :: $3 }
;
expression_list:
| expression { [$1] }
| expression COMMA expression_list { $1 :: $3 }
;
addop:
| LITERAL ADDOP LITERAL { generate_literal ($1 + $3) }
| expression ADDOP expression { (depth_incr generate_add) $1 $3 }
;

subop:
| LITERAL SUBOP LITERAL { generate_literal ($1 - $3) }
| expression SUBOP expression { (depth_incr generate_sub) $1 $3 }
;

expression:
| IDENTIFIER { var $1 }
| LITERAL { generate_literal $1 }
| addop { $1 }
| subop { $1 }
;

statements:
|{}
|statement SEMICOLON statemtns {}

statement:
|IDENRIFIER ASSIGN expression {generate_assign $1 $3; depth_rest }
|READ LEFTPAREN identifier_list RIGHTPAREN { generate_reads $3 }
|WRITE LEFTPAREN expression_list RIGHTPAREN { generate_writes $3; depth_reset }
;


